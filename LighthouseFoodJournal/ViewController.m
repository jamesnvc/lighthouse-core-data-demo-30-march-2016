//
//  ViewController.m
//  LighthouseFoodJournal
//
//  Created by James Cash on 30-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
#import <CoreData/CoreData.h>
#import "ViewController.h"
#import "AppDelegate.h"
#import "Foodstuff.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *foodNameField;
@property (weak, nonatomic) IBOutlet UITextField *foodCaloriesField;
@property (weak, nonatomic) IBOutlet UITextField *servingSizeField;
@property (weak, nonatomic) IBOutlet UILabel *totalFoodsLabel;
@property (weak, nonatomic) IBOutlet UITableView *foodsTable;

@property (strong,nonatomic) NSArray<Foodstuff*>* allFoods;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self displayTotalFoodsCount];
    [self fetchAllFoods];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addFood:(id)sender {
    AppDelegate* del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    // Can't just alloc/init a managed object - use NSEntityDescription
    Foodstuff *newFood = [NSEntityDescription insertNewObjectForEntityForName:@"Foodstuff" inManagedObjectContext:del.managedObjectContext];
    newFood.name = self.foodNameField.text;
    newFood.calories = self.foodCaloriesField.text.doubleValue;
    newFood.servingSize = self.servingSizeField.text;

    [del saveContext];

    self.foodCaloriesField.text = @"";
    self.foodNameField.text = @"";
    self.servingSizeField.text = @"";

    [self displayTotalFoodsCount];
    [self fetchAllFoods];
    [self.foodsTable reloadData];
}

- (NSInteger)totalNumberOfFoods
{
    AppDelegate* del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:@"Foodstuff"];
    NSArray *results = [del.managedObjectContext executeFetchRequest:req error:nil];
    return [results count];
}

- (void)fetchAllFoods
{
    AppDelegate* del = (AppDelegate*)[UIApplication sharedApplication].delegate;
    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:@"Foodstuff"];
    req.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES] ];
    self.allFoods = [del.managedObjectContext executeFetchRequest:req error:nil];
}

- (void)displayTotalFoodsCount
{
    self.totalFoodsLabel.text = [NSString stringWithFormat:@"Storing %ld foods", [self totalNumberOfFoods]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.allFoods.count;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FoodStuffCellIdent"];

    Foodstuff *food = self.allFoods[indexPath.row];

    UILabel *nameLabel = [cell viewWithTag:1];
    UILabel *caloriesLabel = [cell viewWithTag:2];

    nameLabel.text = food.name;
    caloriesLabel.text = [NSString stringWithFormat:@"%f calories per %@", food.calories, food.servingSize];

    return cell;
}

@end
