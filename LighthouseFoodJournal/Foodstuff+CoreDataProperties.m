//
//  Foodstuff+CoreDataProperties.m
//  LighthouseFoodJournal
//
//  Created by James Cash on 30-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Foodstuff+CoreDataProperties.h"

@implementation Foodstuff (CoreDataProperties)

@dynamic name;
@dynamic calories;
@dynamic servingSize;
@dynamic eaten;

@end
