//
//  ViewController.h
//  LighthouseFoodJournal
//
//  Created by James Cash on 30-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource>


@end

