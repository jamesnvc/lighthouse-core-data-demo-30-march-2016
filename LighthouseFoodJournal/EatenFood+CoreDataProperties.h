//
//  EatenFood+CoreDataProperties.h
//  LighthouseFoodJournal
//
//  Created by James Cash on 30-03-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EatenFood.h"

NS_ASSUME_NONNULL_BEGIN

@interface EatenFood (CoreDataProperties)

@property (nonatomic) double servingsEaten;
@property (nonatomic) NSTimeInterval eatenAt;
@property (nullable, nonatomic, retain) NSManagedObject *foodEaten;

@end

NS_ASSUME_NONNULL_END
